---
title: 'Arma 3 Transport Birds'
date: 2019-02-11T19:30:08+10:00
draft: false
weight: 3
summary: Notes on Different Transport Birds in Arma 3, how each can be used, and where to use them
---

_Disclaimer_ : This page will not contain documentation for Hacthet H-60 mod. That will be a separate page since it's features are massive and beyond the scope of this document. It will be linked though so that people can access it.

The bread and butter of Hawkeye, these birds transport the ground troops to and from AOs, carry OPDEMs, provide light CAS with their miniguns and reconnaisance. These birds also carry your vehicles with capabilities to paradrop them where you want them. With more training we will be paradroping the ground troops from these very helicopters.

# MH-6M Littlebird

Want to land in a street, land in the street.
Want to land on a pier, land on the pier.
Want to land on a roof, land on the roof.
Want to land in a clearing, land in the clearing.
Want to land in a backyard, land in the backyard.
Want to rappel troops, rappel the troops.
Want to land on a penguin, land next to it and salute the penguin.
Want to pick up light stuff, pick up the light stuff.
Want to recon, pick up a co-pilot.
Want to shoot stuff, wrong variant.

A very versatile helicopter with the ability to land in very tricky and tight landing zones. Although vunerable to ground fire, its manoeuvrability allows it to escape extremely fast.

Can carry 7 passengers plus one co-pilot. 6 passengers sit on the benches with the ability to fire in their sectors, with the 7th passenger sitting in the fuselage of the chopper.

The bubble cockpit provides nice visibility while making approaches to the landing zone and also for scouting potential landing zones.

The radar can only pick up emitters. The movement direction of the target is not displayed. If there is a yellow circle around the target, it means it's ranging you and a bright orange cone means that is now actively targeting you. Start taking evasie manoeuvres the moment it there is a yellow circle. Advised to be start evading the moment the target pops up on the radar. Most probably it might be AA/AAA.

Weirdly enough it can pick up friendly airplanes with their directions. Must be QoL feature for the helicopter from the base game. 

# RHS UH-60

The workhorse for Hawkeye in troop movements. Can be configured as both a MEDEVAC and CASECAV bird. It can carry upto 13 passengers. Self defense weapons include two mini-guns on the side along with fire from vehicle seats[need the cargo doors open for this]. 

Capability of sling loading smaller vehicles and a boat. Troops can rappel down from each side using the ACE fast roping mechanism which comes default with this helicopter.

This helicopter is less manoeuvrable than the LittleBird. It cannot go into all the tight-spots, such as rooftop landings. It's high speed makes up for the loss in manoeuvrability allowing the helicopter to come into a drop zone and leave the AO faster than the littlebird, decreasing the time spent in the AO.

Very simple flight models in both SFM and AFM. The flight models are based on the vanilla helicopters.

Visibility from the cockpit is limited due to the control panel present in front of the pilot. To the side, the visibility is better. So while making an approach the pilot can come in with a 10 degree offset to the left.

The helicopter only contains a radar warning receiver which works similar to the one in the LittleBird.

# CH-47F Chinook

One of the biggest helicopters in our arsenal. It can carry 24 passengers along with more cargo than the Blackhawk. It is also the fastest helicopter present in our arsenal. Can slingload a humvee underneath with passengers in both the Chinook and the humvee.

For self defence, it has two mini-guns mounted on the side, close to the pilot cabin. No rear gun. It has both Anti-Radar Chaff as well as thermal countermeasures. Use them while landing and takeoff. 

This helicopter is not manoeuvrable at all. It is quite sluggish to controls and if the speed takes off, it can take some time to bleed off that excess speed, with a side effect of increase in altitude. Speed can also bleed off quite fast due to the massive rotors slowing you down when flying at 50 knots[~100km/h]

Visiblity is extremely restircted due to the size and make of the helicopter itsef. The best way to approach any landing zone is to either have a co-pilot guiding you down or come in with an offset. 

Again only contains a radar warning receiver which functions the same way as the Littlebird.

The Chinook also comes in a cargo variant which can take 4-5 passengers with more Vehcile-In-Vehicle cargo space.[Think 2 buggys with all of Sabre]

The **CH-53E Sea Stallion** is the Naval and Marine version of the Sea Stallion.